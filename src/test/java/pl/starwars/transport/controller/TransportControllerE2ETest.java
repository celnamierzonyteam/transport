package pl.starwars.transport.controller;

import org.assertj.core.api.Assert;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import pl.starwars.transport.model.Planet;
import pl.starwars.transport.model.Transport;
import pl.starwars.transport.model.TransportRepository;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class TransportControllerE2ETest {
    @LocalServerPort
    private int port;

    @Autowired
    TestRestTemplate restTemplate;

    @Autowired
    TransportRepository repository;

    @Test
    void TransportControllerE2E_AddingTransport() {
        Transport[] beforeAdding = restTemplate.getForObject("http://localhost:" + port + "/transport", Transport[].class);
        assertEquals(0, beforeAdding.length);

        Transport transportToAdd = new Transport();
        transportToAdd.setFrom(new Planet());
        transportToAdd.setTo(new Planet());
        transportToAdd.getFrom().setId(1);
        transportToAdd.getTo().setId(2);

        Transport addedTransport = restTemplate.postForObject("http://localhost:" + port + "/transport/",
                transportToAdd, Transport.class);
        assertEquals(21, addedTransport.getId().length());

        Transport[] afterAdding = restTemplate.getForObject("http://localhost:" + port + "/transport", Transport[].class);
        assertEquals(1, afterAdding.length);

        Transport resultOfOne = restTemplate.getForObject("http://localhost:" + port + "/transport/" + addedTransport.getId(), Transport.class);
        assertEquals(resultOfOne.getId(), afterAdding[0].getId());
    }
}
