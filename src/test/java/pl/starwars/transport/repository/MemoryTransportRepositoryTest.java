package pl.starwars.transport.repository;

import org.junit.jupiter.api.Test;
import pl.starwars.transport.model.Item;
import pl.starwars.transport.model.Transport;

import java.util.Optional;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.*;

class MemoryTransportRepositoryTest {

    @Test
    void findAll_withEmptyHashMap_shouldReturnNoTransports() {
        MemoryTransportRepository repository = new MemoryTransportRepository();

        Set<Transport> result = repository.findAll();

        assertEquals(0, result.size());
    }

    @Test
    void findAll_withOneElementInHashMap() {
        MemoryTransportRepository repository = new MemoryTransportRepository();
        Transport transport = new Transport();
        repository.save(transport);

        Set<Transport> result = repository.findAll();

        assertEquals(1, result.size());
    }

    @Test
    void findById_withOneElementInHashMap_shouldReturnOptionalWithResult() {
        MemoryTransportRepository repository = new MemoryTransportRepository();
        Transport transport = new Transport();
        repository.save(transport);

        Optional<Transport> result = repository.findById(transport.getId());

        assertTrue(result.isPresent());
    }

    @Test
    void addItem_withOneElementInHashMap_shouldAddItem() {
        MemoryTransportRepository repository = new MemoryTransportRepository();
        Transport transport = new Transport();
        repository.save(transport);

        assertEquals(0, transport.getItems().size());
        Item item = new Item();
        repository.addItem(transport.getId(), item);
        assertEquals(1, transport.getItems().size());
    }

    @Test
    void removeItem_withOneElementInHashMap_shouldAddItem() {
        MemoryTransportRepository repository = new MemoryTransportRepository();
        Transport transport = new Transport();
        repository.save(transport);

        Item item = new Item();
        item.setItemId("3X45");
        repository.addItem(transport.getId(), item);

        int itemsSize = transport.getItems().size();

        repository.removeItem(transport.getId(), item.getItemId());
        assertEquals(itemsSize - 1, transport.getItems().size());
    }

    @Test
    void isExists_whenTransportDoesNotExist_shouldReturnFalse() {
        MemoryTransportRepository repository = new MemoryTransportRepository();

        boolean result = repository.isExists("ddddd");

        assertFalse(result);
    }

    @Test
    void isExists_whenTransportExists_shouldReturnTrue() {
        MemoryTransportRepository repository = new MemoryTransportRepository();
        Transport transport = new Transport();
        repository.save(transport);

        boolean result = repository.isExists(transport.getId());

        assertTrue(result);
    }
}