package pl.starwars.transport.configuration;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

import java.util.List;

@Configuration
@ConfigurationProperties("item")
public class ItemConfiguration {
    public List<String> availableProducers;

    public List<String> getAvailableProducers() {
        return availableProducers;
    }

    void setAvailableProducers(List<String> availableProducers) {
        this.availableProducers = availableProducers;
    }
}
