package pl.starwars.transport.service;

import org.springframework.cache.annotation.Cacheable;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.stereotype.Service;
import pl.starwars.transport.starwarsapi.StarWarsAPIClient;
import pl.starwars.transport.starwarsapi.StarWarsPlanet;
import pl.starwars.transport.model.Planet;

import java.util.ArrayList;
import java.util.List;

@Service
@EnableCaching
public class PlanetService {
    private final StarWarsAPIClient apiClient;

    public PlanetService(StarWarsAPIClient apiClient) {
        this.apiClient = apiClient;
    }

    @Cacheable(cacheNames = "planetsByPage", key = "#page")
    public List<Planet> getPlanetsOnPage(int page) {
        List<StarWarsPlanet> response = apiClient.findPlanetsByPage(page);

        List<Planet> result = new ArrayList<>();
        response.forEach(starWarsPlanet -> {
            Planet planet = new Planet();
            planet.setName(starWarsPlanet.getName());
            planet.setId(starWarsPlanet.getId());
            result.add(planet);
        });
        return result;
    }

    @Cacheable(cacheNames = "planetsById", key = "#id")
    public Planet getPlanetById(int id) {
        StarWarsPlanet response = apiClient.findPlanetById(id);

        Planet result = new Planet();
        result.setName(response.getName());
        result.setId(response.getId());
        return result;
    }
}
