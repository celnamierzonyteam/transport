package pl.starwars.transport.controller;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import pl.starwars.transport.model.Planet;
import pl.starwars.transport.service.PlanetService;

import java.util.List;

@RestController
public class PlanetController {

    private PlanetService planetService;

    public PlanetController(PlanetService planetService) {
        this.planetService = planetService;
    }

    @GetMapping("/planets/page/{page}")
    ResponseEntity<List<Planet>> getPlanetsByPage(@PathVariable int page) {
        return ResponseEntity.ok(planetService.getPlanetsOnPage(page));
    }

    @GetMapping("/planets/{id}")
    ResponseEntity<Planet> getPlanetById(@PathVariable int id) {
        return ResponseEntity.ok(planetService.getPlanetById(id));
    }
}
