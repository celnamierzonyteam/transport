package pl.starwars.transport.controller;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import pl.starwars.transport.model.Item;
import pl.starwars.transport.model.Planet;
import pl.starwars.transport.model.Transport;
import pl.starwars.transport.model.TransportRepository;
import pl.starwars.transport.service.PlanetService;

import javax.validation.Valid;
import java.net.URI;
import java.util.Set;

@RestController
public class TransportController {
    private final TransportRepository transportRepository;
    private final PlanetService planetService;

    public TransportController(TransportRepository transportRepository, PlanetService planetService) {
        this.transportRepository = transportRepository;
        this.planetService = planetService;
    }

    @PostMapping("transport")
    Transport createTransport(@RequestBody @Valid Transport transport) {
        transport.getFrom().setName(
                planetService.getPlanetById(transport.getFrom().getId()).getName());

        transport.getTo().setName(
                planetService.getPlanetById(transport.getTo().getId()).getName());

        return transportRepository.save(transport);
    }

    @GetMapping("transport")
    Set<Transport> getAllTransport() {
        return transportRepository.findAll();
    }

    @GetMapping("transport/{id}")
    ResponseEntity<Transport> getTransport(@PathVariable String id) {
        return transportRepository.findById(id)
                .map(ResponseEntity::ok)
                .orElse(ResponseEntity.notFound().build());
    }

    @PostMapping("transport/{id}/item")
    ResponseEntity<?> addItem(@PathVariable String id, @RequestBody @Valid Item item) {
        if(transportRepository.isExists(id)) {
            return ResponseEntity.notFound().build();
        }

        transportRepository.addItem(id, item);
        return ResponseEntity.created(URI.create("transport/" + id)).build();
    }

}
