package pl.starwars.transport.repository.discount;

import org.springframework.stereotype.Component;
import pl.starwars.transport.model.Item;

import java.util.List;

@Component
interface Discount {
    int calculateDiscount(List<Item> items, int totalCost);
}
