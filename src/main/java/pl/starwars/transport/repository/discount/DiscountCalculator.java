package pl.starwars.transport.repository.discount;

import pl.starwars.transport.model.Item;

import java.util.List;

public class DiscountCalculator implements Discount {
    @Override
    public int calculateDiscount(List<Item> items, int totalCost) {
        int totalDiscount = 0;
        totalDiscount += new FiveProductsDiscount().calculateDiscount(items, totalCost);
        totalDiscount += new FoodDiscount().calculateDiscount(items, totalCost);
        totalDiscount += new HolowanMechanicalsDiscount().calculateDiscount(items, totalCost);
        return totalDiscount;
    }
}
