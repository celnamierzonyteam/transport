package pl.starwars.transport.repository.discount;

import pl.starwars.transport.model.Item;
import pl.starwars.transport.model.ProductType;

import java.util.List;

class FoodDiscount implements Discount {
    final double DISCOUNT_VALUE = 0.3;

    @Override
    public int calculateDiscount(List<Item> items, int totalCost) {
        for (Item item : items) {
            if(item.getProductType() != ProductType.FOOD) {
                return 0;
            }
        }

        return (int) Math.round(totalCost * DISCOUNT_VALUE);
    }
}
