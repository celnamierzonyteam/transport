package pl.starwars.transport.repository.discount;

import pl.starwars.transport.model.Item;

import java.util.List;

class HolowanMechanicalsDiscount implements Discount {

    final String PRODUCER_NAME = "Holowan Mechanicals";

    @Override
    public int calculateDiscount(List<Item> items, int totalCost) {
        int holowanItemsCount = 0, holowanProductCost = 0;

        for (Item item : items) {
            if(item.getProducer() != null && item.getProducer().equals(PRODUCER_NAME)) {
                holowanItemsCount++;
                holowanProductCost += item.getTransportCost();
            }
        };

        if(holowanItemsCount < 2) {
            return 0;
        }

        return (int) Math.round(holowanProductCost * 0.2);
    }
}
