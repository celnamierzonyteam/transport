package pl.starwars.transport.repository.discount;

import pl.starwars.transport.model.Item;

import java.util.List;

class FiveProductsDiscount implements Discount {
    @Override
    public int calculateDiscount(List<Item> items, int totalCost) {
        if(items.size() < 5 ) {
            return 0;
        }
        return (int) Math.round(totalCost * 0.1);
    }
}
