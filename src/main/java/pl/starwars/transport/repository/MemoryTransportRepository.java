package pl.starwars.transport.repository;

import com.aventrix.jnanoid.jnanoid.NanoIdUtils;
import org.springframework.stereotype.Repository;
import pl.starwars.transport.model.Item;
import pl.starwars.transport.model.Transport;
import pl.starwars.transport.model.TransportRepository;
import pl.starwars.transport.repository.discount.DiscountCalculator;

import java.util.*;

@Repository
public class MemoryTransportRepository implements TransportRepository {
    private final HashMap<String, Transport> map = new HashMap<>();

    @Override
    public Set<Transport> findAll() {
        return new HashSet<>(map.values());
    }

    @Override
    public Optional<Transport> findById(String id) {
        return Optional.of(map.get(id));
    }

    @Override
    public Transport save(Transport transport) {
        if(transport.getId() == null || transport.getId().isEmpty()) {
            transport.setId(NanoIdUtils.randomNanoId());
        }

        map.put(transport.getId(), transport);
        return transport;
    }

    @Override
    public void addItem(String id, Item item) {
        map.computeIfPresent(id, (s,t) -> {
            t.getItems().add(item);
            t.setTotalCost(t.getTotalCost() + item.getTransportCost());

            calculateTotalCostWithDiscount(t);
            return t;
        });
    }

    @Override
    public void removeItem(String transportId, String itemId) {
        map.computeIfPresent(transportId, (s,t) -> {
            for (Item item : t.getItems()) {
                if(item.getItemId().equals(itemId)) {
                    t.setTotalCost(t.getTotalCost() + item.getTransportCost());
                    t.getItems().remove(item);

                    calculateTotalCostWithDiscount(t);
                    return t;
                }
            }
            return t;
        });
    }

    @Override
    public boolean isExists(String id) {
        return map.containsKey(id);
    }

    private void calculateTotalCostWithDiscount(Transport transport) {
        DiscountCalculator discountCalculator = new DiscountCalculator();
        transport.setTotalCostWithDiscount(
                transport.getTotalCost() - discountCalculator.calculateDiscount(transport.getItems(), transport.getTotalCost()));
    }
}
