package pl.starwars.transport.model;

public enum ProductType {
    RAW_MATERIALS,
    FOOD,
    ELECTRONIC_COMPONENTS,
    ANIMALS,
    PLANTS
}
