package pl.starwars.transport.model;

import java.util.Optional;
import java.util.Set;

public interface TransportRepository {
    Set<Transport> findAll();
    Optional<Transport> findById(String id);

    Transport save(Transport transport);

    void addItem(String id, Item item);

    void removeItem(String transportId, String itemId);

    boolean isExists(String id);
}
