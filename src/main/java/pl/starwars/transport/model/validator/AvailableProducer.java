package pl.starwars.transport.model.validator;

import javax.validation.Constraint;
import javax.validation.Payload;

import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.FIELD;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

@Target({ FIELD })
@Retention(RUNTIME)
@Constraint(validatedBy = ProducerValidator.class)
@Documented
public @interface AvailableProducer {
    String message() default "{Producer.invalid}";

    Class<?>[] groups() default { };

    Class<? extends Payload>[] payload() default { };
}
