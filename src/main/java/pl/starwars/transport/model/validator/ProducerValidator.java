package pl.starwars.transport.model.validator;

import org.springframework.stereotype.Component;
import pl.starwars.transport.configuration.ItemConfiguration;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

@Component
class ProducerValidator implements ConstraintValidator<AvailableProducer, String> {
    ItemConfiguration itemConfiguration;

    public ProducerValidator(ItemConfiguration itemConfiguration) {
        this.itemConfiguration = itemConfiguration;
    }

    @Override
    public boolean isValid(String s, ConstraintValidatorContext constraintValidatorContext) {
        if(s == null) {
            return true;
        }

        return itemConfiguration.availableProducers.contains(s);
    }
}
