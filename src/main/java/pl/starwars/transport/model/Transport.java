package pl.starwars.transport.model;

import org.springframework.boot.jackson.JsonComponent;

import java.util.LinkedList;
import java.util.List;

@JsonComponent
public class Transport {
    private String id;
    private Planet from;
    private Planet to;
    private List<Item> items = new LinkedList<>();
    private int totalCost;
    private int totalCostWithDiscount;

    public Transport() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Planet getFrom() {
        return from;
    }

    public void setFrom(Planet from) {
        this.from = from;
    }

    public Planet getTo() {
        return to;
    }

    public void setTo(Planet to) {
        this.to = to;
    }

    public List<Item> getItems() {
        return items;
    }

    void setItems(List<Item> items) {
        this.items = items;
    }

    public int getTotalCost() {
        return totalCost;
    }

    public void setTotalCost(int totalCost) {
        this.totalCost = totalCost;
    }

    public int getTotalCostWithDiscount() {
        return totalCostWithDiscount;
    }

    public  void setTotalCostWithDiscount(int totalCostWithDiscount) {
        this.totalCostWithDiscount = totalCostWithDiscount;
    }
}
