package pl.starwars.transport.model;

import org.springframework.boot.jackson.JsonComponent;
import pl.starwars.transport.model.validator.AvailableProducer;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;

@JsonComponent
public class Item {
    @NotBlank
    private String itemId;

    @AvailableProducer
    private String producer;

    private ProductType productType;

    @Min(10)
    @Max(10000000)
    private int weight;

    private int transportCost;

    public String getItemId() {
        return itemId;
    }

    public void setItemId(String itemId) {
        this.itemId = itemId;
    }

    public String getProducer() {
        return producer;
    }

    void setProducer(String producer) {
        this.producer = producer;
    }

    public ProductType getProductType() {
        return productType;
    }

    void setProductType(ProductType productType) {
        this.productType = productType;
    }

    public int getWeight() {
        return weight;
    }

    void setWeight(int weight) {
        this.weight = weight;
    }

    public int getTransportCost() {
        return transportCost;
    }

    void setTransportCost(int transportCost) {
        this.transportCost = transportCost;
    }
}
