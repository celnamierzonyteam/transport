package pl.starwars.transport.starwarsapi;

import org.springframework.boot.jackson.JsonComponent;

@JsonComponent
public class StarWarsPlanet {
    private String name;
    private String url;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public int getId() {
        String[] splitedUrl = url.split("/");
        return Integer.parseUnsignedInt(splitedUrl[splitedUrl.length - 1]);
    }
}
