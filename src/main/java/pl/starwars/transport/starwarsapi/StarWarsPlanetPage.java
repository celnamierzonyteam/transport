package pl.starwars.transport.starwarsapi;

import org.springframework.boot.jackson.JsonComponent;

import java.util.List;

@JsonComponent
public class StarWarsPlanetPage {
    private List<StarWarsPlanet> results;

    public List<StarWarsPlanet> getResults() {
        return results;
    }

    public void setResults(List<StarWarsPlanet> results) {
        this.results = results;
    }
}
