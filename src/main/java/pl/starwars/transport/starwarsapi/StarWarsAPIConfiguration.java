package pl.starwars.transport.starwarsapi;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Configuration
@ConfigurationProperties("starwarsapi")
public class StarWarsAPIConfiguration {
    private String host;

    public String getHost() {
        return host;
    }

    void setHost(String host) {
        this.host = host;
    }
}
