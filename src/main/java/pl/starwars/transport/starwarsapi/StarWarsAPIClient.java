package pl.starwars.transport.starwarsapi;

import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Mono;

import java.util.List;

@Component
public class StarWarsAPIClient {

    private final StarWarsAPIConfiguration configuration;

    private final WebClient apiPlanetClient;

    public StarWarsAPIClient(StarWarsAPIConfiguration configuration) {
        this.configuration = configuration;

        this.apiPlanetClient = WebClient.create(configuration.getHost());
    }

    public List<StarWarsPlanet> findPlanetsByPage(int page) {
        Mono<StarWarsPlanetPage> listOfPlanets = apiPlanetClient.get()
                .uri("/planets/?page={page}", page)
                .retrieve()
                .bodyToMono(StarWarsPlanetPage.class);

        return listOfPlanets.block().getResults();
    }

    public StarWarsPlanet findPlanetById(int id) {
        Mono<StarWarsPlanet> starWarsPlanet = apiPlanetClient.get()
                .uri("/planets/{id}", id)
                .retrieve()
                .bodyToMono(StarWarsPlanet.class);
        return starWarsPlanet.block();
    }
}
